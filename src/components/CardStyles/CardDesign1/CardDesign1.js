import React, {Component} from 'react';
import Grid from 'material-ui/Grid';
import { withStyles } from 'material-ui/styles';
import PropTypes from 'prop-types';
import Card, { CardActions, CardContent, CardMedia } from 'material-ui/Card';
import Typography from 'material-ui/Typography';
import './cardDesign1.css';



const styles = theme => ({
    grid1: {
       
       background:'rgb(55, 63, 72)',
      },
      card: {
        maxWidth: 345,
        boxShadow:'none',
      },
      media: {
        height: 0,
        paddingTop: '56.25%', // 16:9
      },
      cardContent:{
        paddingLeft:'0px',

      },
      cardHeadText:{
        fontFamily: '"Fjalla One", sans-serif',
      },
      cardContentText:{
        lineHeight:'1.8em',
      }
     
});



class CardDesign1 extends Component{
    constructor(props){

        super(props);
    }

	  render() {
          const {classes} = this.props;
      
    return (

    <div>
          <Card className={classes.card}>
        <CardMedia
          className={classes.media}
          image="/Assets/images/industry1.jpg"
          title="Contemplative Reptile"
        />
        <CardContent className={classes.cardContent}>
          <Typography gutterBottom variant="headline" component="h2" className={classes.cardHeadText}>
            {this.props.content.tittle}
          </Typography>
          <Typography component="p" className={classes.cardContentText}>
          {this.props.content.Content}
          </Typography>
        </CardContent>

      </Card>
    </div>

    );
    
 
  }
}

CardDesign1.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(CardDesign1);