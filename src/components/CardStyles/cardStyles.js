import React, {Component} from 'react';
import CardDesign1 from './CardDesign1/CardDesign1';
import CardDesign2 from './CardDesign2/CardDesign2';
import Grid from 'material-ui/Grid';
import Slider from "react-slick";
import './cardStyles.css';


import { withStyles } from 'material-ui/styles';
import PropTypes from 'prop-types';

const styles = theme => ({
  SlickContainerBox:{
   padding:'0px 10px'
  }
}); 

class cardStyles extends Component{
  constructor(props){
    super(props)
  } 

	  render() {
      const settings = {

        
        infinite: false,
        speed: 300,
        slidesToShow: 3,
        slidesToScroll: 1,
       
        Arrows: true,
        responsive: [
          {
            breakpoint: 768,
            settings: {
          
              slidesToShow: 2
            }
          },
          {
            breakpoint: 480,
            settings: {
          
              slidesToShow: 1
            }
          }
        ]
    
    };
    const {classes} = this.props;

      let cardContent = {
        tittle:"Manufacturing ",
        Content: "With our professional security expertise, we provide a complete, connected approach to safeguarding what matters most to you and your organization."
      }
      let cardContent1 = {
        tittle:"Industries",
        Content: "We serve a vast array of industries with a strong focus 'campus' environments. From Construction to Manufacturing."
      }
      let cardContent2 = {
        tittle:"Our Team",
        Content: "We employ some of the most well respected consultants in the country who are often sought out and industry knowledge-leaders. "
      }
    return (
      <div>
      <h1>Intro Cards for Shafi Chacha web</h1>
      <Grid container spacing={0}>
      <Grid item xs>
      </Grid>
          <Grid item xs={10}>
          
          <Grid container spacing={40} >
        <Grid item xs={10} sm={6} md={4}>
        <CardDesign1  content={cardContent}/>
        </Grid>
        <Grid item xs={10} sm={6}  md={4}>
        <CardDesign1  content={cardContent1}/>
        </Grid>
        <Grid item xs={10} sm={6}  md={4}>
        <CardDesign1  content={cardContent2}/>
        </Grid>

      </Grid>
          </Grid>
          <Grid item xs>
           

          </Grid>


      </Grid>
      <h1>Product Cards for Shafi Chacha web</h1>
      <Grid container spacing={0}>
        <Grid item xs></Grid>
          <Grid item xs={10}>
          <Slider {...settings}> 
                    <div >
                    <div className={classes.SlickContainerBox}>
                    <CardDesign2  content={cardContent}/>
                    </div>
                      </div>
                      <div >
                    <div className={classes.SlickContainerBox}>
                    <CardDesign2  content={cardContent}/>
                    </div>
                      </div>
                      <div >
                    <div className={classes.SlickContainerBox}>
                    <CardDesign2  content={cardContent}/>
                    </div>
                      </div>
                      <div >
                    <div className={classes.SlickContainerBox}>
                    <CardDesign2  content={cardContent}/>
                    </div>
                      </div>
                      <div >
                    <div className={classes.SlickContainerBox}>
                    <CardDesign2  content={cardContent}/>
                    </div>
                      </div>
                      <div >
                    <div className={classes.SlickContainerBox}>
                    <CardDesign2  content={cardContent}/>
                    </div>
                      </div>
                   
          </Slider>

          </Grid>

        <Grid item xs></Grid>

      </Grid>


       <h1>Testimonial Cards for Shafi Chacha web</h1>

      
 


     

</div>
    );
  }
}

cardStyles.propTypes = {
  classes : PropTypes.object.isRequired,
};
export default withStyles(styles)(cardStyles);
