import React, {Component} from 'react';
import Grid from 'material-ui/Grid';
import { withStyles } from 'material-ui/styles';
import PropTypes from 'prop-types';
import SvgIcon from 'material-ui/SvgIcon';
import Divider from 'material-ui/Divider';
import Button from 'material-ui/Button';

import './FooterDesign2.css';


const styles = theme => ({
    grid1: {
       
       background:'#202020',
      },
      grid2: {
       
        background:'#202020',
        textAlign:'left',
       },
       button: {
       marginTop:'10px'
      },
      footerContainer:{
        // textAlign: "center",
      },
      CompanyStyle:{
        color: 'white',
       
      
        margin: '0px',
      },
      headText:{
       
        color: '#fff',
        font: '14px "Open Sans", sans-serif',
        fontWeight: 'bolder',
        marginBottom:'10px',
        margin:'0px',
        [theme.breakpoints.down('xs')] :{
            fontSize:'4.3vw',
        }
      },
      linkText:{
        font: '14px "Open Sans", sans-serif',
        color: '#cbcbcb',
        // textTransform:'uppercase',
        paddingBottom: '4px',
        display: 'inline-block',

        '&:hover': {
            color: 'white',
        },
      },
      copyrightText:{
        font: '12px "Open Sans", sans-serif',
        color:'white',
        
        display:'inline-block',
      },
      copyrightSectionLinks: {
        color: 'white',
        display: 'inline-block',
        font: '12px "Open Sans", sans-serif',
        paddingLeft: '8px',
        paddingRight: '8px',
        borderRight: '1px solid white',
        '&:last-child':{
            borderRight: 'none',
        }
      },
      copyrightLinksContainer:{
padding:'22px 0px'
      },
      socialIcons:{
          margin:'0px 10px',
          padding: '22px 0px',
         
      }
    
});

function HomeIcon(props) {
    return (
      <SvgIcon x="0px" y="0px"
      width="430.113px" height="430.114px" viewBox="0 0 430.113 430.114" style={{enableBackground:'new 0 0 430.113 430.114'}}
     {...props}>

  
    <path d="M158.081,83.3c0,10.839,0,59.218,0,59.218h-43.385v72.412h43.385v215.183h89.122V214.936h59.805
		c0,0,5.601-34.721,8.316-72.685c-7.784,0-67.784,0-67.784,0s0-42.127,0-49.511c0-7.4,9.717-17.354,19.321-17.354
		c9.586,0,29.818,0,48.557,0c0-9.859,0-43.924,0-75.385c-25.016,0-53.476,0-66.021,0C155.878-0.004,158.081,72.48,158.081,83.3z"/>  </SvgIcon>
    );
  }

  function GoogleIcon (props) {
      return (
<SvgIcon version="1.1" id="Capa_1"  x="0px" y="0px"
	 width="96.828px" height="96.827px" viewBox="0 0 96.828 96.827" style={{enableBackground:'new 0 0 96.828 96.827'}}
	 {...props}>
<g>
	<g>
		<path d="M62.617,0H39.525c-10.29,0-17.413,2.256-23.824,7.552c-5.042,4.35-8.051,10.672-8.051,16.912
			c0,9.614,7.33,19.831,20.913,19.831c1.306,0,2.752-0.134,4.028-0.253l-0.188,0.457c-0.546,1.308-1.063,2.542-1.063,4.468
			c0,3.75,1.809,6.063,3.558,8.298l0.22,0.283l-0.391,0.027c-5.609,0.384-16.049,1.1-23.675,5.787
			c-9.007,5.355-9.707,13.145-9.707,15.404c0,8.988,8.376,18.06,27.09,18.06c21.76,0,33.146-12.005,33.146-23.863
			c0.002-8.771-5.141-13.101-10.6-17.698l-4.605-3.582c-1.423-1.179-3.195-2.646-3.195-5.364c0-2.672,1.772-4.436,3.336-5.992
			l0.163-0.165c4.973-3.917,10.609-8.358,10.609-17.964c0-9.658-6.035-14.649-8.937-17.048h7.663c0.094,0,0.188-0.026,0.266-0.077
			l6.601-4.15c0.188-0.119,0.276-0.348,0.214-0.562C63.037,0.147,62.839,0,62.617,0z M34.614,91.535
			c-13.264,0-22.176-6.195-22.176-15.416c0-6.021,3.645-10.396,10.824-12.997c5.749-1.935,13.17-2.031,13.244-2.031
			c1.257,0,1.889,0,2.893,0.126c9.281,6.605,13.743,10.073,13.743,16.678C53.141,86.309,46.041,91.535,34.614,91.535z
			 M34.489,40.756c-11.132,0-15.752-14.633-15.752-22.468c0-3.984,0.906-7.042,2.77-9.351c2.023-2.531,5.487-4.166,8.825-4.166
			c10.221,0,15.873,13.738,15.873,23.233c0,1.498,0,6.055-3.148,9.22C40.94,39.337,37.497,40.756,34.489,40.756z"/>
		<path d="M94.982,45.223H82.814V33.098c0-0.276-0.225-0.5-0.5-0.5H77.08c-0.276,0-0.5,0.224-0.5,0.5v12.125H64.473
			c-0.276,0-0.5,0.224-0.5,0.5v5.304c0,0.275,0.224,0.5,0.5,0.5H76.58V63.73c0,0.275,0.224,0.5,0.5,0.5h5.234
			c0.275,0,0.5-0.225,0.5-0.5V51.525h12.168c0.276,0,0.5-0.223,0.5-0.5v-5.302C95.482,45.446,95.259,45.223,94.982,45.223z"/>
	</g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
</SvgIcon>
      );
  }

  function TwitterIcon (props){
      return(
<SvgIcon version="1.1" id="Capa_1"  x="0px" y="0px"
	 viewBox="0 0 612 612" style={{enableBackground:'new 0 0 612 612'}} {...props}>
<g>
	<g>
		<path  d="M612,116.258c-22.525,9.981-46.694,16.75-72.088,19.772c25.929-15.527,45.777-40.155,55.184-69.411
			c-24.322,14.379-51.169,24.82-79.775,30.48c-22.907-24.437-55.49-39.658-91.63-39.658c-69.334,0-125.551,56.217-125.551,125.513
			c0,9.828,1.109,19.427,3.251,28.606C197.065,206.32,104.556,156.337,42.641,80.386c-10.823,18.51-16.98,40.078-16.98,63.101
			c0,43.559,22.181,81.993,55.835,104.479c-20.575-0.688-39.926-6.348-56.867-15.756v1.568c0,60.806,43.291,111.554,100.693,123.104
			c-10.517,2.83-21.607,4.398-33.08,4.398c-8.107,0-15.947-0.803-23.634-2.333c15.985,49.907,62.336,86.199,117.253,87.194
			c-42.947,33.654-97.099,53.655-155.916,53.655c-10.134,0-20.116-0.612-29.944-1.721c55.567,35.681,121.536,56.485,192.438,56.485
			c230.948,0,357.188-191.291,357.188-357.188l-0.421-16.253C573.872,163.526,595.211,141.422,612,116.258z"/>
	</g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
</SvgIcon>
      );
  }


  function CopyRight (props){
    return (

        <SvgIcon version="1.1"  width="16" height="16" viewBox="0 0 16 16" {...props} > 
<path  d="M8 1.5c3.6 0 6.5 2.9 6.5 6.5s-2.9 6.5-6.5 6.5-6.5-2.9-6.5-6.5 2.9-6.5 6.5-6.5zM8 0c-4.4 0-8 3.6-8 8s3.6 8 8 8 8-3.6 8-8-3.6-8-8-8v0z"></path>
<path  d="M9.9 10.3c-0.5 0.4-1.2 0.7-1.9 0.7-1.7 0-3-1.3-3-3s1.3-3 3-3c0.8 0 1.6 0.3 2.1 0.9l1.1-1.1c-0.8-0.8-2-1.3-3.2-1.3-2.5 0-4.5 2-4.5 4.5s2 4.5 4.5 4.5c1.1 0 2-0.4 2.8-1l-0.9-1.2z"></path>
</SvgIcon>

    );
  }

class FooterDesign2 extends Component{
    constructor(props){

        super(props);
    }

	  render() {
          const {classes} = this.props;
      
    return (
        <div>
      <Grid className={classes.grid1 + " " + classes.footerContainer } container spacing={0}
          alignItems={"start"}
          direction={"row"}
          justify={"center"}
        >
        <Grid item xs={1}> </Grid>
        <Grid item xs={10}>
        <Grid style={{margin:'36px 0px'}}  container spacing={0}
          alignItems={"start"}
          direction={"row"}
          justify={"center"}>
              <Grid className={classes.grid2}  item xs={12} md={5} sm={5} style={{paddingRight:'44px', textAlign:'justify'}}>
      <h1 className={classes.headText} >What we do</h1>
    <span className={classes.linkText}>We are a growing Company based in Dubai we are involved in manufacturing sales of xyz products in Xyz <a href="#" style={{textDecoration:'none', color:'#f33f00'}}>industries</a>  and various reputable Brands like Schnieder and others.</span><br/><br/>
    <span className={classes.linkText}>We'd love to collabrate with you. Just send us a message and will get back to you.</span><br/>
    
    <Button variant="raised" color="secondary" className={classes.button}>
        Enquire
      </Button>
    </Grid>
      <Grid className={classes.grid2}  item xs={12} md sm style={{paddingRight:'28px'}} >
      <h1 className={classes.headText} >Contact</h1>
    <span className={classes.linkText}>Abu Dubai Street</span><br/>
    <span className={classes.linkText}>+97 9494494847</span><br/>
    <span className={classes.linkText} >fax +8349384948</span><br/>
    <span className={classes.linkText} >info@company.com</span><br/>
    </Grid>
   
  
    <Grid className={classes.grid2}  item xs={12} md sm  > 
    <h1 className={classes.headText} >Products</h1>
    <span className={classes.linkText}>Dampers </span><br/>
    <span className={classes.linkText} >Grills & Registers</span><br/>
    <span className={classes.linkText} >Ceiling Diffusers</span><br/>
    <span className={classes.linkText} >Show All</span><br/>
    </Grid>
    <Grid className={classes.grid2}  item xs={12} md sm>
    <h1 className={classes.headText} >Brands</h1>
    <span className={classes.linkText}>Schneider </span><br/>
    <span className={classes.linkText} >Beghelli</span><br/>
    <span className={classes.linkText} >Clipsal-Conducts</span><br/>
    <span className={classes.linkText} >Show All</span><br/>
    </Grid>

        </Grid>
  
        </Grid>
        <Grid item xs={1}> </Grid>
     

    <Grid item xs={1}></Grid>
    <Grid className={classes.grid1}  item xs={10}>
    <Divider light style={{background:'#676767'}} />
    <Grid container spacing={0} direction={'row'} alignItems={'flex-start'} justify={'space-between'}>
<Grid item xs={12} sm={8} md={8} lg={8}>
<div className={classes.copyrightLinksContainer}>
<span className={classes.copyrightText}> <CopyRight style={{fontSize:'9' , color:'white', marginLeft:'18px'}}/> 2018 Awesome Company </span>
<span className={classes.copyrightSectionLinks}>About </span>
<span className={classes.copyrightSectionLinks}>Contact</span>
<span className={classes.copyrightSectionLinks}>Careers</span>
<span className={classes.copyrightSectionLinks}>Products</span>
</div>
</Grid>
<Grid item xs={12} sm={4} md={4} lg={4} style={{textAlign:'right'}}>
<span className={classes.linkText + " " + classes.socialIcons} ><HomeIcon style={{ fontSize: '18',}}/></span>
    <span className={classes.linkText + " " + classes.socialIcons} ><TwitterIcon style={{ fontSize: '18',}} /></span>
    <span className={classes.linkText + " " + classes.socialIcons} ><GoogleIcon style={{ fontSize: '18',}} /></span>
</Grid>

    </Grid>
   
  
    
    </Grid>
    <Grid item xs={1}></Grid>
      </Grid>
      </div>
    );
  }
}

FooterDesign2.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(FooterDesign2);