import React, {Component} from 'react';
import Grid from 'material-ui/Grid';
import { withStyles } from 'material-ui/styles';
import PropTypes from 'prop-types';
import FooterDesign1 from './FooterDesign1/FooterDesign1';
import FooterDesign2 from './FooterDesign2/FooterDesign2';
import './footerstyles.css';

const styles = theme => ({

    
});

class FooterStyles extends Component{
    constructor(props){

        super(props);
    }

	  render() {
          const {classes} = this.props;
      
    return (
    <div>
           <h1>design for shafi chacha web 1</h1>
        <FooterDesign1/>
        <br/>
        <h1>design for shafi chacha web</h1>
        <FooterDesign2/>
      </div>
    );
  }
}

FooterStyles.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(FooterStyles);