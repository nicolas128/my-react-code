import Header from './Header/Header';
import Footer from './Footer/Footer';
import Home from './Home/Home';
import HeaderStyles from './HeaderStyles/HeaderStyles';
import FooterStyles from './FooterStyles/FooterStyles';
import HomeBannerStyles from './HomeBannerStyles/HomeBannerStyles';
import cardStyles from './CardStyles/cardStyles';
import UtilityComponents from  './UtilityComponents/UtilityComponents';
import Pages from  './Pages/Pages';


export {Header,Footer, Home , HeaderStyles , FooterStyles , HomeBannerStyles, cardStyles, UtilityComponents, Pages}

