import React, { Component } from 'react';
import Grid from 'material-ui/Grid';
import { withStyles } from 'material-ui/styles';
import PropTypes from 'prop-types';
import TextField from 'material-ui/TextField';
import Typography from 'material-ui/Typography';
import Button from 'material-ui/Button';
import Divider from 'material-ui/Divider';
import './ContactDesign1.css';



const styles = theme => ({

  container: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  textField: {

   
  },
  headText: {
    fontFamily: 'Montserrat',
    fontWeight: 'normal',
    color: '#333333',
    marginBottom: '12px',
    letterSpacing: '-0.2px',
    fontSize: '18px',
  },
  paraText: {
    color: '#777777',
    fontSize: '16px',
    fontFamily: '"PT Sans"',
    lineHeight: '1.7',


  },
  routingText:{
    color: '#777777',
    fontSize: '12px',
    fontFamily: '"PT Sans"',
    lineHeight: '1.7',
    marginRight: '24px',

  },
  paraText1: {
    color: '#777777',
    fontSize: '16px',
    fontFamily: '"PT Sans"',
    lineHeight: '1.7',
    marginTop: '2em',
    display: 'inline-block',



  },
  TittleText: {
    fontFamily: 'Montserrat',
    fontWeight: 'bold',
  },
  TittleDash: {
    borderBottom: '2px solid orange',
    width: '76px'
  },
  paraTextBox:{
    paddingTop:'67px',
    paddingRight:'20px'
  },
  arrowText:{
    fontFamily: "Neuton",
    color: '#d5d5d5',
    fontSize: '24px',
    padding: '0px 20px',
  }

});



class ContactDesign1 extends Component {
  constructor(props) {

    super(props);
  }

  render() {
    const { classes } = this.props;

    return (

      <div>
        <Grid container spacing={24}>
        <Grid item xs={12} >
        <div style={{padding:'12px 38px'}}>
        <span className={classes.routingText} style={{fontWeight:'bold'}}> Company</span> 
        <span className={classes.arrowText}> ></span> 
        <span className={classes.routingText} > > Contact Us</span>
        </div>
        <Divider/>
        </Grid>
        
        </Grid>
      
        <Grid container spacing={0}>
   
        <Grid item xs></Grid>

          <Grid item md={5}>
          
           
            <form className={classes.container} noValidate autoComplete="off">
            <Grid container spacing={24}  style={{margin:'0px'}}>
            <Grid item xs={12}>
            <div>
              <h2 className={classes.TittleText}>Contact Us</h2>
              <div className={classes.TittleDash}></div>
            </div>
            <span className={classes.paraText1}> Please use the form below to email your questions to us!</span>
            </Grid>
            <Grid item xs={6}>
            <TextField
                id="First Name"
                label="First Name *"
                className={classes.textField}
                margin="normal"
                fullWidth
              />
            </Grid>

               <Grid item xs={6}>
               <TextField
                id="Last Name"
                label="Last Name *"
                className={classes.textField}
                margin="normal"
                fullWidth
              />
            </Grid>
            <Grid item xs={6}>
            <TextField
                id="Email"
                label="Your Email"
                className={classes.textField}
                margin="normal"
                fullWidth
              />
            </Grid>

               <Grid item xs={6}>
               <TextField
                id="Phone"
                label="Your Phone Number *"
                className={classes.textField}
                margin="normal"
                fullWidth
              />

            </Grid>
            <Grid item xs={12}>
            
            <TextField
                id="Subject"
                label="Subject"
                className={classes.textField}
                fullWidth
                margin="normal"
              />
            </Grid>

            <Grid item xs={12}>
            
            <TextField
                id="Message"
                label="Message"
                className={classes.textField}
                fullWidth
                margin="normal"
                multiline
                rows="4"
              />
             </Grid>

             <Grid item xs={12}>
             <Button style={{backgroundColor: '#ffa500'}} variant="raised" color="secondary"  className={classes.button}>
        Send Feedback
      </Button>

             </Grid>
            </Grid>
              
             
             
             
        

       
            </form>

          </Grid>

          <Grid item xs></Grid>
          <Grid item md={5}>
            <Grid container spacing={0}>
              <Grid item xs={6} className={classes.paraTextBox}>
                <Typography className={classes.headText} variant="subheading" gutterBottom>
                  MAILING ADDRESS
      </Typography>
                <Typography className={classes.paraText} align="justify" variant="body1" gutterBottom>
                  Prag Group of Industries<br />
                  B-1 Talkatora Industrial Estate<br />
                  Lucknow – 226011. India<br />
                </Typography>
              </Grid>

              <Grid item item xs={6} className={classes.paraTextBox}>
                <Typography className={classes.headText} variant="subheading" gutterBottom>
                  CONTACT INFO
      </Typography>
                <Typography className={classes.paraText} align="justify" variant="body1" gutterBottom>
                  Phone: +91 (522) 3980 300/307/308<br />
                  Fax: +91 (522) 3980 309<br />
                  <span style={{ color: '#fab702' }}>corporate@praggroup.com</span>
                </Typography>

              </Grid>

            </Grid>



          </Grid>

          <Grid item xs></Grid>


        </Grid>




      </div>

    );


  }
}

ContactDesign1.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(ContactDesign1);