import React, {Component} from 'react';
import Grid from 'material-ui/Grid';
import { withStyles } from 'material-ui/styles';
import PropTypes from 'prop-types';
import Card, { CardActions, CardContent, CardMedia } from 'material-ui/Card';
import Typography from 'material-ui/Typography';
import './AboutDesign1.css';



const styles = theme => ({
    grid1: {
       
       background:'rgb(55, 63, 72)',
      },
      card: {
        maxWidth: 345,
        boxShadow:'none',
      },
      media: {
        height: 0,
        paddingTop: '56.25%', // 16:9
      },
      cardContent:{
        paddingLeft:'0px',

      },
      cardHeadText:{
        fontFamily: '"Fjalla One", sans-serif',
      },
      cardContentText:{
        lineHeight:'1.8em',
      }
     
});



class AboutDesign1 extends Component{
    constructor(props){

        super(props);
    }

	  render() {
          const {classes} = this.props;
      
    return (

    <div>
       <h1> About Us Page</h1>
    </div>

    );
    
 
  }
}

AboutDesign1.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(AboutDesign1);