import React, {Component} from 'react';

import Grid from 'material-ui/Grid';
import Slider from "react-slick";
import './Pages.css';

import ContactDesign1 from './ContactDesign1/ContactDesign1';
import AboutDesign1 from './AboutDesign1/AboutDesign1';


import { withStyles } from 'material-ui/styles';
import PropTypes from 'prop-types';

const styles = theme => ({
  SlickContainerBox:{
   padding:'0px 10px'
  }
}); 

class Pages extends Component{
  constructor(props){
    super(props)
  } 

	  render() {

    const {classes} = this.props;

  
    return (
      <div>
      <h1>Pages Templates</h1>
      <h1>Contact Design 1 </h1>
      <ContactDesign1/> 

       <h1>About Us Design 1 </h1>
      <AboutDesign1/> 



      </div>
    );
  }
}

Pages.propTypes = {
  classes : PropTypes.object.isRequired,
};
export default withStyles(styles)(Pages);
