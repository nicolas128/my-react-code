import React, { Component } from 'react';
import Grid from 'material-ui/Grid';

import IconButton from 'material-ui/IconButton';
import { Paper, Button } from 'material-ui';

import SvgIcon from 'material-ui/SvgIcon';
import Hidden from 'material-ui/Hidden';

import green from 'material-ui/colors/green';





import { NavLink, Redirect } from 'react-router-dom';


import { withStyles } from 'material-ui/styles';
import PropTypes from 'prop-types';

const styles = theme => ({
    root: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'flex-end',
    },
    head2text: {
        fontSize: '2.75em',
        color: 'white',
        letterSpacing: '4px',
        fontFamily: '"proxima-nova","Arial",sans-serif',
        fontWeight: '700',
        textTransform: 'uppercase',
        lineHeight: '120%',

        marginTop: '1rem',
        marginBottom: '1rem',
    },
    p2: {
        fontFamily: '"Source Sans Pro", sans-serif',
        color: 'white',
        fontWeight: '300',
        fontSize: '1.5rem',
        lineHeight: '160%',

        [theme.breakpoints.down('xs')]: {
            fontSize: '1.1rem',
            color: '#ececec',
            lineHeight: '119%',

        },
    },
    paperBlock: {
        padding: theme.spacing.unit * 2,
        textAlign: 'left',
        color: theme.palette.text.secondary,
        background: 'linear-gradient(to right, #38373c85, #44434800)',
        paddingLeft: '5em'
    },


});



class HomeBannerDesign2 extends Component {
    constructor(props) {
        super(props);



    }




    render() {
        const { classes } = this.props;

       
        return (
            <div>

                <Grid container spacing={0} direction={'column'} alignItems={'flex-start'} justify={'flex-start'} style={{

                    background: 'url(/Assets/images/25.jpg)',
                    height: this.props.height,
                    backgroundSize: 'cover',
                    backgroundRepeatX: 'no-repeat',
                    backgroundPositionX: 'right',
                }}>
                    <Grid item xs>


                    </Grid>

                    <Grid item xs={7} md={6}>
                        <Paper className={classes.paperBlock} elevation={0} style={{ height: '29em' }} >

                            <Grid style={{ height: '100%' }} container spacing={0} direction={'column'} alignItems={'left'} justify={'center'}>
                                <Grid item xs></Grid>
                                <Grid item xs>


                                    <h1 className={classes.head2text}>{this.props.ContentParameters.subText}</h1>
                                    <p className={classes.p2}>{this.props.ContentParameters.subText}
                                    </p>

                                </Grid>
                                <Grid item xs></Grid>
                            </Grid>
                        </Paper>

                    </Grid>


                    <Grid item xs>


                    </Grid>






                </Grid>

            </div>
        );
    }
}
HomeBannerDesign2.propTypes = {
    classes: PropTypes.object.isRequired,
};
export default withStyles(styles)(HomeBannerDesign2);