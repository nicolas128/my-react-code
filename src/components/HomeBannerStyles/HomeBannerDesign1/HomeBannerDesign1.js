import React, {Component} from 'react';
import Grid from 'material-ui/Grid';
import Button from 'material-ui/Button';
import IconButton from 'material-ui/IconButton';

import SvgIcon from 'material-ui/SvgIcon';



import { NavLink, Redirect } from 'react-router-dom';


import { withStyles } from 'material-ui/styles';
import PropTypes from 'prop-types';

const styles = theme => ({
    root: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'flex-end',
      },
      BannerImage: {
        height: '100%',
        width: '100%',
        background: 'url(./Assets/images/25.jpg)',
        backgroundSize: 'cover',
        backgroundPosition: 'center center',
       
    },
    BannerTextContainer:{
      
        padding:'42px 50px',

    textAlign: 'center',
    background:' #6d676738',
    },
    headText:{
        fontSize:'4rem',
        color:'white',
        textShadow: '1px 1px 2px #847979'
    },
    hText:{
        color:'white',
        textShadow: '1px 1px 2px #847979',
        fontSize: '16px',
    }
   
 
});



class HomeBannerDesign1 extends Component{
    constructor(props) {
        super(props);
   

  
    }

  


	  render() {
          const {classes} = this.props;


    return (
     <div>
<div className={classes.BannerImage}>
<div className={classes.BannerTextContainer}>


<h1 className={classes.headText}> Change is my dream</h1>
<p className={classes.hText}> Get the skills and confidence needed to succeed in times of change and complexity.</p>

</div>

</div>
 


     </div>
    );
  }
}
HomeBannerDesign1.propTypes = {
    classes : PropTypes.object.isRequired,
};
export default withStyles(styles)(HomeBannerDesign1);