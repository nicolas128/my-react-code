import React, {Component} from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import { NavLink } from 'react-router-dom';
import './Header.css';

const styles = theme => ({
  navLink: {
      color: 'blue !important',
      [theme.breakpoints.down('xs')]: {
          color: 'green',
      }
  }

});

class Header extends Component{
  constructor(props) {
    super(props);
}

	  render() {
      const { classes } = this.props;
    return (
      <div>
<NavLink className={classes.navLink} style={{ textDecoration: 'none', color: 'inherit', underline: 'none' }} to="/headerStyle"> Header Styles</NavLink>
<NavLink className={'navLink2'} style={{ textDecoration: 'none', color: 'inherit', underline: 'none' }} to="/footerStyle"> Footer Styles</NavLink>
<NavLink className={'navLink2'} style={{ textDecoration: 'none', color: 'inherit', underline: 'none' }} to="/homeBannerStyle"> Home Banner Styles</NavLink>
<NavLink className={'navLink2'} style={{ textDecoration: 'none', color: 'inherit', underline: 'none' }} to="/cardStyles"> card Styles</NavLink>
<NavLink className={'navLink2'} style={{ textDecoration: 'none', color: 'inherit', underline: 'none' }} to="/utilityComponents"> Utility Components </NavLink>
<NavLink className={'navLink2'} style={{ textDecoration: 'none', color: 'inherit', underline: 'none' }} to="/pages"> Pages</NavLink>

      </div>
    
    );
  }
}

Header.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Header);

