import React, {Component} from 'react';
import { withStyles } from 'material-ui/styles';
import PropTypes from 'prop-types';
import StickyComponent from './StickyComponent/StickyComponent';
import StickyComponentHide from './StickyComponentHide/StickyComponentHide'; 


const styles = theme => ({
  root: {
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'flex-end',
    },
 
   
   
});


class UtilityComponents extends Component{

	  render() {
      const {classes} = this.props

    return (
      <div>


      <StickyComponent className="sticky-three" >
      <h1 style={{margin:'0px'}}>Sticky Component Example</h1>  <StickyComponentHide className="sticky-threde" > <p>hide me </p> </StickyComponentHide>
      </StickyComponent>

      <div style={{height:'60em' , width:'100%'}}>
      
      </div>
      </div>
    );
  }
}

UtilityComponents.propTypes = {
  classes : PropTypes.object.isRequired,
};
export default withStyles(styles)(UtilityComponents);