import React, {Component} from 'react';
import Grid from 'material-ui/Grid';
import { withStyles } from 'material-ui/styles';
import PropTypes from 'prop-types';
import SvgIcon from 'material-ui/SvgIcon';
import Grow from 'material-ui/transitions/Grow';



const styles = theme => ({
    grid1: {
       
       background:'rgb(55, 63, 72)',
      },
      Hide:{
    visibility:'hidden'
    
      },
      Show:{
        visibility:'visible',
     
      }
    
});

class StickyComponentHide extends Component{
    constructor(props){

        super(props);
        this.state = {
          show:false,
        }
      
    }

    componentDidMount() {

      const {classes} = this.props;
      
        const setInitialHeights = (elements) => {
          [].forEach.call(elements, (sticky) => {
            sticky.setAttribute('data-sticky-initial1', sticky.getBoundingClientRect().top);
          });
        };
    
        const stickies = document.querySelectorAll('[data-sticky1]');
        setInitialHeights(stickies);


        document.addEventListener('scroll', () => {
            const top = document.documentElement.scrollTop || document.body.scrollTop;
            const bottom = document.documentElement.scrollHeight || document.body.scrollHeight;
      
            [].forEach.call(stickies, (sticky) => {
              const stickyInitial = parseInt(sticky.getAttribute('data-sticky-initial1'), 10);
              const stickyEnter = parseInt(sticky.getAttribute('data-sticky-enter1'), 10) || stickyInitial;
              const stickyExit = parseInt(sticky.getAttribute('data-sticky-exit1'), 10) || bottom;
   
              if (top >= stickyEnter && top <= stickyExit) {
              
                  // sticky.classList.add(classes.Show);
                  this.setState({
                    show: true,
                  });
                  console.log('state of the component' + this.state.show)
              
              } else {
                // sticky.classList.remove(classes.Show);
                this.setState({
                  show: false,
                });
                console.log('state of the component' + this.state.show)
              }


            });
          });
      }

	  render() {
          const {classes, className, enter, exit, children} = this.props;
      
    return (
        <div>
    <Grow in={this.state.show}

style={{ transformOrigin: '0 0 0' }}
{...(this.state.show ? { timeout: 1000 } : {})}

     className={  className}
     data-sticky1
     data-sticky1-enter={enter}
     data-sticky1-exit={exit}
   
    >
   <div
       
 
      >
    
        {children}
  
      </div>
</Grow>
      </div>
    );
  }
}

StickyComponentHide.propTypes = {
    classes: PropTypes.object.isRequired,
    className: PropTypes.string,
    enter: PropTypes.string,
    exit: PropTypes.string,
    children: PropTypes.node,
};

export default withStyles(styles)(StickyComponentHide);

