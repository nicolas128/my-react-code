import React, {Component} from 'react';
import Grid from 'material-ui/Grid';
import { withStyles } from 'material-ui/styles';
import PropTypes from 'prop-types';
import SvgIcon from 'material-ui/SvgIcon';
import './StickyComponent.css';

const styles = theme => ({
    grid1: {
       
       background:'rgb(55, 63, 72)',
      },
      sticky:{
        position: 'fixed',
        top: '0',
        left: '0',
        right: '0',
        borderRadius: '0',
        margin: '0',
    
      }
    
});

class StickyComponent extends Component{
    constructor(props){

        super(props);
      
    }

    componentDidMount() {

      const {classes} = this.props;
      
        const setInitialHeights = (elements) => {
          [].forEach.call(elements, (sticky) => {
            sticky.setAttribute('data-sticky-initial', sticky.getBoundingClientRect().top);
          });
        };
    
        const stickies = document.querySelectorAll('[data-sticky]');
        setInitialHeights(stickies);


        document.addEventListener('scroll', () => {
            const top = document.documentElement.scrollTop || document.body.scrollTop;
            const bottom = document.documentElement.scrollHeight || document.body.scrollHeight;
      
            [].forEach.call(stickies, (sticky) => {
              const stickyInitial = parseInt(sticky.getAttribute('data-sticky-initial'), 10);
              const stickyEnter = parseInt(sticky.getAttribute('data-sticky-enter'), 10) || stickyInitial;
              const stickyExit = parseInt(sticky.getAttribute('data-sticky-exit'), 10) || bottom;
      
              if (top >= stickyEnter && top <= stickyExit) {
                sticky.classList.add(classes.sticky);
             
              } else {
                sticky.classList.remove(classes.sticky);
              }
            });
          });
      }

	  render() {
          const {classes, className, enter, exit, children } = this.props;
      
    return (
        <div>

   <div
        className={`Sticky ${className}`}
        data-sticky
        data-sticky-enter={enter}
        data-sticky-exit={exit}
      >
        {children}
      </div>
      </div>
    );
  }
}

StickyComponent.propTypes = {
    classes: PropTypes.object.isRequired,
    className: PropTypes.string,
    enter: PropTypes.string,
    exit: PropTypes.string,
    children: PropTypes.node,
};

export default withStyles(styles)(StickyComponent);

