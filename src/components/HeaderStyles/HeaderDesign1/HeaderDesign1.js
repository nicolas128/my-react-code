import React, {Component} from 'react';
import Grid from 'material-ui/Grid';
import Button from 'material-ui/Button';
import IconButton from 'material-ui/IconButton';
import classNames from 'classnames';

import Input from 'material-ui/Input';
import Hidden from 'material-ui/Hidden';
import SvgIcon from 'material-ui/SvgIcon';
import green from 'material-ui/colors/green';
import List, { ListItem, ListItemIcon, ListItemText } from 'material-ui/List';

import { Manager, Target, Popper } from 'react-popper';
import ClickAwayListener from 'material-ui/utils/ClickAwayListener';
import Collapse from 'material-ui/transitions/Collapse';
import Grow from 'material-ui/transitions/Grow';
import Paper from 'material-ui/Paper';
import Portal from 'material-ui/Portal';

import { AppBar, Toolbar, Typography} from 'material-ui';
import './HeaderDesign1.css';
import Sticky from '../../UtilityComponents/StickyComponent/StickyComponent';
import StickyComponentHide from '../../UtilityComponents/StickyComponentHide/StickyComponentHide'; 



import Drawer from 'material-ui/Drawer';

import { withStyles, MuiThemeProvider } from 'material-ui/styles';
import PropTypes from 'prop-types';

const styles = theme => ({
   
        button:{
            textDecoration: 'none',
            fontSize: '16px',
            textTransform: 'capitalize',
            color: '#312b2b', 
            paddingTop:'1.3em',
            paddingBottom: '1.3em'
        },
        headerTop: {
            background:'#424242',
        },
        hideMenu:{
            display:'none',
        },
        hTittleText:{
            color:'white',
            paddingLeft:'20px',
            display:'inline-block'
        },
        hsubText:{
            color:'white',
            paddingLeft:'8px',
            display:'inline-block'
        },
        menuParagraph:{
            margin:'0px',
            marginTop:'5px',
        },
        icon:{
            padding:'20px'
        },
        icon2:{

        },
        stickyone: {
          
        },
        menuDialog:{
            position: 'absolute',
            top: '64px',
            left: '0px',
            height: '99px',
            background: 'red',
        },
        logo2Box:{
            paddingTop:'5px',
            [theme.breakpoints.down('sm')] :{
               paddingTop:'0px'
            }
        }
    
});

function HomeIcon(props) {
    return (
      <SvgIcon {...props}>
    <path d="M0 0h24v24H0z" fill="none"/>
    <path d="M3 18h18v-2H3v2zm0-5h18v-2H3v2zm0-7v2h18V6H3z"/>
      </SvgIcon>
    );
  }

  function CallUs(props) {
    return (
        <SvgIcon id="Capa_1" x="0px" y="0px"
        width="348.077px" height="348.077px" viewBox="0 0 348.077 348.077" style={{enableBackground: "new 0 0 348.077 348.077"}}
         {...props}>
   <g>
       <g>
           <g>
               <path d="M340.273,275.083l-53.755-53.761c-10.707-10.664-28.438-10.34-39.518,0.744l-27.082,27.076
                   c-1.711-0.943-3.482-1.928-5.344-2.973c-17.102-9.476-40.509-22.464-65.14-47.113c-24.704-24.701-37.704-48.144-47.209-65.257
                   c-1.003-1.813-1.964-3.561-2.913-5.221l18.176-18.149l8.936-8.947c11.097-11.1,11.403-28.826,0.721-39.521L73.39,8.194
                   C62.708-2.486,44.969-2.162,33.872,8.938l-15.15,15.237l0.414,0.411c-5.08,6.482-9.325,13.958-12.484,22.02
                   C3.74,54.28,1.927,61.603,1.098,68.941C-6,127.785,20.89,181.564,93.866,254.541c100.875,100.868,182.167,93.248,185.674,92.876
                   c7.638-0.913,14.958-2.738,22.397-5.627c7.992-3.122,15.463-7.361,21.941-12.43l0.331,0.294l15.348-15.029
                   C350.631,303.527,350.95,285.795,340.273,275.083z"/>
           </g>
       </g>
   </g>
   <g>
   </g>
   <g>
   </g>
   <g>
   </g>
   <g>
   </g>
   <g>
   </g>
   <g>
   </g>
   <g>
   </g>
   <g>
   </g>
   <g>
   </g>
   <g>
   </g>
   <g>
   </g>
   <g>
   </g>
   <g>
   </g>
   <g>
   </g>
   <g>
   </g>
   </SvgIcon>
    );
  }

  function Email(props) {
    return (
        <SvgIcon version="1.1" id="Capa_1" x="0px" y="0px"
        width="511.626px" height="511.626px" viewBox="0 0 511.626 511.626" style={{enableBackground:"new 0 0 511.626 511.626"}}
        {...props}>
   <g>
       <g>
           <path d="M49.106,178.729c6.472,4.567,25.981,18.131,58.528,40.685c32.548,22.554,57.482,39.92,74.803,52.099
               c1.903,1.335,5.946,4.237,12.131,8.71c6.186,4.476,11.326,8.093,15.416,10.852c4.093,2.758,9.041,5.852,14.849,9.277
               c5.806,3.422,11.279,5.996,16.418,7.7c5.14,1.718,9.898,2.569,14.275,2.569h0.287h0.288c4.377,0,9.137-0.852,14.277-2.569
               c5.137-1.704,10.615-4.281,16.416-7.7c5.804-3.429,10.752-6.52,14.845-9.277c4.093-2.759,9.229-6.376,15.417-10.852
               c6.184-4.477,10.232-7.375,12.135-8.71c17.508-12.179,62.051-43.11,133.615-92.79c13.894-9.703,25.502-21.411,34.827-35.116
               c9.332-13.699,13.993-28.07,13.993-43.105c0-12.564-4.523-23.319-13.565-32.264c-9.041-8.947-19.749-13.418-32.117-13.418H45.679
               c-14.655,0-25.933,4.948-33.832,14.844C3.949,79.562,0,91.934,0,106.779c0,11.991,5.236,24.985,15.703,38.974
               C26.169,159.743,37.307,170.736,49.106,178.729z"/>
           <path d="M483.072,209.275c-62.424,42.251-109.824,75.087-142.177,98.501c-10.849,7.991-19.65,14.229-26.409,18.699
               c-6.759,4.473-15.748,9.041-26.98,13.702c-11.228,4.668-21.692,6.995-31.401,6.995h-0.291h-0.287
               c-9.707,0-20.177-2.327-31.405-6.995c-11.228-4.661-20.223-9.229-26.98-13.702c-6.755-4.47-15.559-10.708-26.407-18.699
               c-25.697-18.842-72.995-51.68-141.896-98.501C17.987,202.047,8.375,193.762,0,184.437v226.685c0,12.57,4.471,23.319,13.418,32.265
               c8.945,8.949,19.701,13.422,32.264,13.422h420.266c12.56,0,23.315-4.473,32.261-13.422c8.949-8.949,13.418-19.694,13.418-32.265
               V184.437C503.441,193.569,493.927,201.854,483.072,209.275z"/>
       </g>
   </g>
   <g>
   </g>
   <g>
   </g>
   <g>
   </g>
   <g>
   </g>
   <g>
   </g>
   <g>
   </g>
   <g>
   </g>
   <g>
   </g>
   <g>
   </g>
   <g>
   </g>
   <g>
   </g>
   <g>
   </g>
   <g>
   </g>
   <g>
   </g>
   <g>
   </g>
   </SvgIcon>
   
    );
  }

  function ProductIcon(props) {
    return (
      <SvgIcon {...props}>
    <path d="M0 0h24v24H0z" fill="none"/>
    <path d="M3 18h18v-2H3v2zm0-5h18v-2H3v2zm0-7v2h18V6H3z"/>
      </SvgIcon>
    );
  }

  function AboutUsIcon(props) {
    return (
      <SvgIcon {...props}>
    <path d="M0 0h24v24H0z" fill="none"/>
    <path d="M3 18h18v-2H3v2zm0-5h18v-2H3v2zm0-7v2h18V6H3z"/>
      </SvgIcon>
    );
  }

  function ContactUsIcon(props) {
    return (
      <SvgIcon {...props}>
    <path d="M0 0h24v24H0z" fill="none"/>
    <path d="M3 18h18v-2H3v2zm0-5h18v-2H3v2zm0-7v2h18V6H3z"/>
      </SvgIcon>
    );
  }

  function IndustryIcon(props) {
    return (
      <SvgIcon {...props}>
    <path d="M0 0h24v24H0z" fill="none"/>
    <path d="M3 18h18v-2H3v2zm0-5h18v-2H3v2zm0-7v2h18V6H3z"/>
      </SvgIcon>
    );
  }

  function LogoIcon(props) {
    return (
        <SvgIcon viewBox={"22 -8 80 70"}  {...props} style={{ width: '2em', fontSize: '36px'} } >
            <g>
                <path fillRule="evenodd" clipRule="evenodd" fill="#FE2C38" d="M27.938,55.473C12.455,55.506-0.041,43.108,0,27.753
                    C0.04,12.605,12.521,0.084,27.665,0c15.354-0.083,27.822,12.403,27.811,27.853C55.463,43.214,43.273,55.441,27.938,55.473z"/>
	<path fillRule="evenodd" clipRule="evenodd" fill="white" d="M66.68,25.732c0,5.211-0.189,10.432,0.066,15.63
                    c0.173,3.513-2.109,2.602-3.899,2.607c-1.799,0.006-4.001,0.897-3.904-2.611c0.225-8.111,0.139-16.234,0.025-24.351
		c-0.029-2.123,0.593-2.712,2.725-2.747c9.136-0.152,9.138-0.229,11.185,8.48c0.968,4.12,1.924,8.243,3.148,13.49
		c1.584-6.988,3.017-12.909,4.233-18.875c0.48-2.355,1.454-3.146,3.955-3.112c8.569,0.115,8.571-0.003,8.571,8.494
		c0,6.12-0.197,12.249,0.079,18.355c0.145,3.2-1.55,3.087-3.66,2.901c-1.862-0.164-4.376,1.036-4.177-2.756
		c0.272-5.165,0.068-10.356-0.427-15.642c-1.284,5.009-2.762,9.978-3.78,15.039c-0.61,3.036-2.15,3.54-4.898,3.452
		c-2.575-0.084-4.348-0.116-4.964-3.36c-0.965-5.083-2.552-10.048-3.875-15.063C66.949,25.687,66.815,25.71,66.68,25.732z"/>
	<path fillRule="evenodd" clipRule="evenodd" fill="white" d="M117.65,23.316c-3.009,0.551-4.526,0.095-5.465-2.271
                    c-0.653-1.646-2.543-1.9-4.242-1.421c-1.015,0.286-1.655,1.023-1.729,2.185c-0.118,1.846,1.059,2.773,2.499,3.31
		c1.979,0.74,4.078,1.157,6.065,1.878c4.819,1.747,7.093,5.094,6.657,9.601c-0.365,3.779-3.49,6.711-8.14,7.572
		c-3.374,0.626-6.699,0.428-9.962-0.697c-1.693-0.583-3.187-1.478-4.291-2.875c-1.233-1.56-2.429-3.334-1.634-5.359
		c0.422-1.075,6.819-0.06,7.707,1.146c0.606,0.823,1.243,1.571,2.217,1.936c2.267,0.848,4.734,1.178,5.782-1.44
		c1.001-2.502-1.257-3.398-3.195-4.1c-2.454-0.889-5.051-1.496-7.351-2.682c-5.917-3.051-6.17-10.728-0.607-14.351
		c5.549-3.615,14.388-2.139,17.395,2.932c0.763,1.285,1.786,2.797,1.192,4.164C119.891,24.354,118.065,23.187,117.65,23.316z"/>
	<path fillRule="evenodd" clipRule="evenodd" fill="white" d="M37.19,27.341c4.317,3.166,5.506,5.943,4.425,9.743
                    c-0.964,3.388-4.317,6.073-8.456,6.276c-4.599,0.227-9.215,0.057-13.823,0.146c-1.632,0.032-2.14-0.611-2.13-2.199
		c0.056-8.346,0.053-16.693,0.004-25.039c-0.009-1.533,0.376-2.293,2.075-2.249c4.73,0.123,9.478-0.077,14.189,0.259
		c3.277,0.233,5.936,1.92,6.951,5.341C41.406,22.922,40.287,25.467,37.19,27.341z"/>
	<path fillRule="evenodd" clipRule="evenodd" fill="#FE303B" d="M28.285,37.516c-1.999,0.298-3.539,0.056-3.47-3.157
                    c0.061-2.819,1.141-3.353,3.538-3.122c2.281,0.218,5.181-0.499,5.339,3.009C33.852,37.803,31.046,37.481,28.285,37.516z"/>
	<path fillRule="evenodd" clipRule="evenodd" fill="#FE3B46" d="M27.191,25.708c-1.666,0.345-2.26-0.668-2.383-2.918
                    c-0.159-2.902,1.419-2.878,3.431-2.791c2.153,0.094,4.632-0.011,4.449,3.022C32.505,26.056,29.842,25.454,27.191,25.708z"/>
            </g>

        </SvgIcon>
    );

}

function LogoIcon2(props) {
    return (
        <SvgIcon viewBox={"-12 -24 149 100"}  {...props} style={{ width: '2em', height:'100%', fontSize: '36px'} } >
            <g>
                <path fillRule="evenodd" clipRule="evenodd" fill="#FE2C38" d="M27.938,55.473C12.455,55.506-0.041,43.108,0,27.753
                    C0.04,12.605,12.521,0.084,27.665,0c15.354-0.083,27.822,12.403,27.811,27.853C55.463,43.214,43.273,55.441,27.938,55.473z"/>
	<path fillRule="evenodd" clipRule="evenodd" fill="black" d="M66.68,25.732c0,5.211-0.189,10.432,0.066,15.63
                    c0.173,3.513-2.109,2.602-3.899,2.607c-1.799,0.006-4.001,0.897-3.904-2.611c0.225-8.111,0.139-16.234,0.025-24.351
		c-0.029-2.123,0.593-2.712,2.725-2.747c9.136-0.152,9.138-0.229,11.185,8.48c0.968,4.12,1.924,8.243,3.148,13.49
		c1.584-6.988,3.017-12.909,4.233-18.875c0.48-2.355,1.454-3.146,3.955-3.112c8.569,0.115,8.571-0.003,8.571,8.494
		c0,6.12-0.197,12.249,0.079,18.355c0.145,3.2-1.55,3.087-3.66,2.901c-1.862-0.164-4.376,1.036-4.177-2.756
		c0.272-5.165,0.068-10.356-0.427-15.642c-1.284,5.009-2.762,9.978-3.78,15.039c-0.61,3.036-2.15,3.54-4.898,3.452
		c-2.575-0.084-4.348-0.116-4.964-3.36c-0.965-5.083-2.552-10.048-3.875-15.063C66.949,25.687,66.815,25.71,66.68,25.732z"/>
	<path fillRule="evenodd" clipRule="evenodd" fill="black" d="M117.65,23.316c-3.009,0.551-4.526,0.095-5.465-2.271
                    c-0.653-1.646-2.543-1.9-4.242-1.421c-1.015,0.286-1.655,1.023-1.729,2.185c-0.118,1.846,1.059,2.773,2.499,3.31
		c1.979,0.74,4.078,1.157,6.065,1.878c4.819,1.747,7.093,5.094,6.657,9.601c-0.365,3.779-3.49,6.711-8.14,7.572
		c-3.374,0.626-6.699,0.428-9.962-0.697c-1.693-0.583-3.187-1.478-4.291-2.875c-1.233-1.56-2.429-3.334-1.634-5.359
		c0.422-1.075,6.819-0.06,7.707,1.146c0.606,0.823,1.243,1.571,2.217,1.936c2.267,0.848,4.734,1.178,5.782-1.44
		c1.001-2.502-1.257-3.398-3.195-4.1c-2.454-0.889-5.051-1.496-7.351-2.682c-5.917-3.051-6.17-10.728-0.607-14.351
		c5.549-3.615,14.388-2.139,17.395,2.932c0.763,1.285,1.786,2.797,1.192,4.164C119.891,24.354,118.065,23.187,117.65,23.316z"/>
	<path fillRule="evenodd" clipRule="evenodd" fill="white" d="M37.19,27.341c4.317,3.166,5.506,5.943,4.425,9.743
                    c-0.964,3.388-4.317,6.073-8.456,6.276c-4.599,0.227-9.215,0.057-13.823,0.146c-1.632,0.032-2.14-0.611-2.13-2.199
		c0.056-8.346,0.053-16.693,0.004-25.039c-0.009-1.533,0.376-2.293,2.075-2.249c4.73,0.123,9.478-0.077,14.189,0.259
		c3.277,0.233,5.936,1.92,6.951,5.341C41.406,22.922,40.287,25.467,37.19,27.341z"/>
	<path fillRule="evenodd" clipRule="evenodd" fill="#FE303B" d="M28.285,37.516c-1.999,0.298-3.539,0.056-3.47-3.157
                    c0.061-2.819,1.141-3.353,3.538-3.122c2.281,0.218,5.181-0.499,5.339,3.009C33.852,37.803,31.046,37.481,28.285,37.516z"/>
	<path fillRule="evenodd" clipRule="evenodd" fill="#FE3B46" d="M27.191,25.708c-1.666,0.345-2.26-0.668-2.383-2.918
                    c-0.159-2.902,1.419-2.878,3.431-2.791c2.153,0.094,4.632-0.011,4.449,3.022C32.505,26.056,29.842,25.454,27.191,25.708z"/>
            </g>

        </SvgIcon>
    );

}




class HeaderDesign1 extends Component{
    constructor(props) {
        super(props);
        this.state = {
            top: false,
            left: false,
            bottom: false,
            right: false,
            ProdVisible: true,
            open:false,
        };

        this.handleButtonCLickclose = this.handleButtonCLickclose.bind(this);
  
    }

   
    

    
  
   

    toggleDrawer = (side, open) => () => {
        console.log("toggle left bar");
        this.setState({
            [side]: open,
        });
    };

    toggleMenu = () => {
        this.setState({open:!this.state.open});
    }
    handleClose = event => {
        if (this.target1.contains(event.target)) {
          return;
        }
    
        this.setState({ open: false });
      };
    
    handleButtonCLick = () => {
     
        this.setState((prevState)=>{
            console.log('previous state' + prevState.ProdVisible);
            return {ProdVisible: !prevState.ProdVisible};
        });

    }
   handleButtonCLickclose(){
       console.log('close menu');
    this.setState({ProdVisible:true
        });

   }



	  render() {
          const {classes} = this.props;
          const { open } = this.state;

          console.log('rendering' + this.state.ProdVisible);
         
          let menuClass = classNames({
            [classes.hideMenu]:this.state.ProdVisible,
            [classes.menuDialog]:true,
          });
         


    return (
     <div>

            <div className={classes.headerTop}>

            <Grid container spacing={0} direction={'row'} alignItems={'center'} justify={'center'}>
            <Grid item xs></Grid>
            <Grid item xs={2}>

            <LogoIcon className={classes.icon} color="secondary" type="linkedin"  />
            </Grid>
            <Grid item xs={8} style={{textAlign:'right'}}>
            <span className={classes.hTittleText}><CallUs style={{fontSize:'18px'}}/> </span> <span className={classes.hsubText}>+97 9043032958</span>  <span className={classes.hTittleText} ><Email  style={{fontSize:'18px'}} /> </span> <span className={classes.hsubText}>Info@company.com</span>
         
            </Grid>
            <Grid item xs></Grid>
            
            </Grid>
            
        </div>
        
        <Sticky  className={classes.stickyone}>       
<AppBar position="static" color="default" style={{ backgroundColor: 'white' ,}}>
                    <Toolbar>

                         
                        <Grid container spacing={0}>
                        <Hidden only={['md', 'lg']} >
                                <Grid item xs>
                                 
                                    <IconButton onClick={this.toggleDrawer('left', true)} className={classes.menuButton} color="inherit" aria-label="Menu">
                                    <HomeIcon  />
                                    </IconButton>
                               
                            </Grid>
                            </Hidden>
                            <Grid item xs style={{textAlign: 'center'}}>
   <StickyComponentHide  className={classes.logo2Box} >
                            <LogoIcon2 className={classes.icon2} color="secondary" type="linkedin"  />
                            </ StickyComponentHide>
                            </Grid>
                            <Grid item xs={8} style={{textAlign:'center'}}>
                            
                           
                                    <Hidden only={['sm', 'xs']}  >
                                    <Manager>
                                    <Button  className={classes.button} color="inherit"  onClick={this.handleButtonCLick} >Products   <div className={menuClass}  onMouseOut={this.handleButtonCLickclose}>hello </div></Button>
                                        <Button className={classes.button} color="inherit"     >Industries</Button>
                                        <Button  className={classes.button} color="inherit" >Brands</Button>
                                        <Button  className={classes.button} color="inherit">About us</Button>
                                       
          <Target   style={{display:'inline-block'}}>
            <div
          
              ref={node => {
                this.target1 = node;
              }}
            >
         <Button  onClick={this.toggleMenu} className={classes.button} color="inherit">Contact us</Button>

            </div>
          </Target>
          <Popper
            placement="bottom"
            eventsEnabled={open}
            className={classNames({ [classes.popperClose]: !open })}
          >
            <ClickAwayListener onClickAway={this.handleClose}>
              <Grow in={open} id="menu-list-grow" style={{ transformOrigin: '0 0 0' }}>
                <Paper >
         <p className={classes.menuParagraph} >sdlfks</p>
                </Paper>
              </Grow>
            </ClickAwayListener>
          </Popper>
        </Manager>
                              
           
   
                                </Hidden>
                            </Grid> 
                            <Grid item xs></Grid>
                        </Grid>


                    </Toolbar>
                </AppBar>
                </Sticky>       
                <Drawer open={this.state.left} onClose={this.toggleDrawer('left', false)}>
                    <div className={classes.list} >
                            <List component="nav">
                                <ListItem button >
                     
                                <ListItemText primary="Products" />
                            </ListItem>
                                <ListItem button >
                            
                                <ListItemText primary="About Us" />
                            </ListItem>
                                <ListItem button >
                              
                                <ListItemText primary="Contact Us" />
                            </ListItem>

                                <ListItem button >
                         
                                <ListItemText primary="Certifications"  />
                            </ListItem>
                    
                        </List>
                    
                    </div>
                   
                </Drawer>



 

     </div>
    );
  }
}
HeaderDesign1.propTypes = {
    classes : PropTypes.object.isRequired,
};
export default withStyles(styles)(HeaderDesign1);