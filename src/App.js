import React, { Component } from 'react';
import {Header , Footer , Home , HeaderStyles , FooterStyles , HomeBannerStyles, cardStyles , UtilityComponents , Pages} from './components';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import { withStyles } from 'material-ui/styles';
import PropTypes from 'prop-types';

import './App.css';


const styles = theme => ({
  containerSection: {
      marginTop: '65px',
      [theme.breakpoints.down('xs')]: {
          marginTop: '57px',
      }
  }

});


class App extends Component {

  constructor(props) {
    super(props);
}

  render() {
    return (

      <Router>
      <div >
          <Header />
          <div >
              <Route exact path="/" component={Home} />
              <Route path="/headerStyle" component={HeaderStyles} />
              <Route path="/footerStyle" component={FooterStyles} />
              <Route path="/homeBannerStyle" component={HomeBannerStyles} />
              <Route path="/cardStyles" component={cardStyles} />
              <Route path="/utilityComponents" component={UtilityComponents} />
              <Route path="/pages" component={Pages} />

              
              
          </div>
          <Footer />
      </div>
  </Router>

 
    );
  }
}

App.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(App);
